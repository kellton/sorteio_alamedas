<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<title>Resultado Sorteio Habitafor</title>

                <link rel="stylesheet" href="<?php echo base_url('publico/reveal/css/reveal.css')?>">
		<link rel="stylesheet" href="<?php echo base_url('publico/reveal/css/theme/black.css')?>">

		<!-- Theme used for syntax highlighting of code -->
		<link rel="stylesheet" href="<?php echo base_url('publico/reveal/lib/css/zenburn.css')?>">
	</head>
	<body>
		<div class="reveal">
			<div class="slides">
                            <?php
                            $cont=1;
                            echo '<section style="font-size:60px;">';
                            foreach ($resultados as $valor) {
                                if($cont%2==0){
                                    echo '</section>';
                                    echo '<section style="font-size:60px;">';
                                    echo "$valor->nome<br>$valor->quadra-$valor->lote-$valor->bloco-$valor->apartamento<br><br>";
                                }
                                else{
                                    echo "$valor->nome<br>$valor->quadra-$valor->lote-$valor->bloco-$valor->apartamento<br><br>";
                                }
                                $cont++;
                            }
                            ?>
			</div>
		</div>

		<script src="<?php echo base_url('publico/reveal/lib/js/head.min.js')?>"></script>
		<script src="<?php echo base_url('publico/reveal/js/reveal.js')?>"></script>

		<script>		
			Reveal.initialize({
			autoSlide: 5000,
			loop: true,
				dependencies: [
					{ src: '<?php echo base_url('publico/reveal/plugin/markdown/marked.js')?>' },
					{ src: '<?php echo base_url('publico/reveal/plugin/markdown/markdown.js')?>' },
					{ src: '<?php echo base_url('publico/reveal/plugin/notes/notes.js')?>', async: true },
					{ src: '<?php echo base_url('publico/reveal/plugin/highlight/highlight.js')?>', async: true, callback: function() { hljs.initHighlightingOnLoad(); } }
				]
			});
		</script>
	</body>
</html>
