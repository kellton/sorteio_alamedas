<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url('publico/imagens/logoVotacao1.png') ?>"/>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Resultado do Sorteio</title>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="<?php echo base_url("publico/css/bootstrap.min.css")?>" rel="stylesheet">
    <link href="<?php echo base_url("publico/css/menu.css")?>" rel="stylesheet">
    <script src="<?php echo base_url("publico/js/canvasjs.min.js") ?>"></script>    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
      <div class="container">
      <div class="col-md-12">
        <div class="col-sm-4 col-md-4 col-lg-4">
            <div class="logo-principal">
                <img class="img-responsive" style="max-height: 100px;" src="<?php echo base_url('publico/imagens/logo_citinova.jpg') ?>">
            </div>
        </div>
<!--        <div class="col-md-offset-5 col-md-3">
            <div class="logo-principal">
                <img class="img-responsive" src="<?php echo base_url('publico/imagens/logo_secultfor.jpg') ?>">
            </div>
        </div>-->
    </div>
          </div>

    