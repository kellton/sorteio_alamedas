<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container-fluid">
    <div class="col-md-9 col-md-offset-1 col-lg-8 col-lg-offset-2">
        <h3>Resultado do Sorteio das Unidades do LOTEAMENTO CIDADE JARDIM 2 - <?php echo date("d/m/Y")?></h3>
        <div class="table-responsive">
        <?php 
            $tmpl = array (
                'table_open'          => '<table class="table table-striped">',

                'heading_row_start'   => '<tr>',
                'heading_row_end'     => '</tr>',
                'heading_cell_start'  => '<th>',
                'heading_cell_end'    => '</th>',

                'row_start'           => '<tr>',
                'row_end'             => '</tr>',
                'cell_start'          => '<td>',
                'cell_end'            => '</td>',

                'row_alt_start'       => '<tr>',
                'row_alt_end'         => '</tr>',
                'cell_alt_start'      => '<td>',
                'cell_alt_end'        => '</td>',

                'table_close'         => '</table>'
            );

            $this->table->set_template($tmpl);
            $this->table->set_heading('Nome','CPF','NIS','PCD','IDOSO','Quadra','Lote','Bloco','Apartamento');
            foreach ($resultados as $valor) {
                $this->table->add_row($valor->nome,$valor->cpf,$valor->nis,$valor->pcd,$valor->idoso,$valor->quadra,$valor->lote,$valor->bloco,$valor->apartamento);
            }
            echo $this->table->generate();
        ?>
        </div>
    </div>
</div>