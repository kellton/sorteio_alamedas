<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resultado_model extends CI_Model{
    
    public function resultado() {
        $this->db->select('beneficiarios.*,apartamentos.nome AS apartamento,blocos.nome AS bloco, lotes.nome AS lote, quadras.nome AS quadra');
        $this->db->from('resultados');
        $this->db->join('beneficiarios','resultados.beneficiario_id=beneficiarios.id');
        $this->db->join('apartamentos','resultados.apartamento_id=apartamentos.id');
        $this->db->join('blocos','blocos.id=apartamentos.bloco_id');
        $this->db->join('lotes','lotes.id=apartamentos.lote_id');
        $this->db->join('quadras','quadras.id=apartamentos.quadra_id');
        $this->db->order_by('beneficiarios.nome','ASC');
        return $this->db->get()->result();
    }
}