<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mympdf{

    public function generate($html,$nome_arquivo){
        define('DOMPDF_ENABLE_AUTOLOAD', false);
        require_once("./vendor/autoload.php");
        $stylesheet = file_get_contents(base_url() . 'publico/css/tabela.css');
        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4','margin_left'=>'15', 'margin_right'=>'15', 'margin_top'=>'32', 'margin_bottom'=>'35','default_font' => 'monospace','default_font_size' => 7]);
        $mpdf->SetHTMLHeader('
        <table width="100%">
        <tr>
            <td><img style="width: 30%; margin-top:-2%;" src="' . base_url() . 'publico/imagens/logo.jpg"/></td>
            <td>Resultado LOTEAMENTO CIDADE JARDIM 2 - ' .date("d/m/Y").'</td>
        </table><hr>');
        $mpdf->SetHTMLFooter('
<table width="100%">
    <tr>
        <td width="90%">Rua Paula Rodrigues, 304 • Fátima • CEP 60.411-270 • Fortaleza, Ceará, Brasil</td>
        <td width="10%" align="center">{PAGENO}/{nbpg}</td>
    </tr>
</table>');
        $mpdf->WriteHTML($stylesheet,1);
        $mpdf->WriteHTML($html,2);
        $mpdf->Output($nome_arquivo.'.pdf','I');
  }
}