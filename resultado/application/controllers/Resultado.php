<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resultado extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('resultado_model');
        $this->load->library('mympdf');
    }
    
    public function index(){
        $dados['resultados']=$this->resultado_model->resultado();
        $this->load->view('template/header');
        $this->load->view('home',$dados);
        $this->load->view('template/footer');
    }
    
    public function slide_resultado() {
        $dados['resultados']=$this->resultado_model->resultado();
        $this->load->view('slide_resultado',$dados);
    }

    public function pdf(){
        $dados['resultados']=$this->resultado_model->resultado();
        $html=$this->load->view('resultado_pdf', $dados, true);
        $this->mympdf->generate($html,'Resultado do Sorteio das Unidades do LOTEAMENTO CIDADE JARDIM 2');
    }
    
}
