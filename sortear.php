<?php

require_once "bootstrap.php";

$apartamentosRepository = $entityManager->getRepository('Apartamento');
$beneficiariosRepository = $entityManager->getRepository('Beneficiario');

$conn = $entityManager->getConnection();

// DEFICIENTES
$beneficiariosDeficientes = $entityManager->createQuery('SELECT b FROM Beneficiario b WHERE b.pcd = \'Sim\' '
                 . ' AND '
                 . '(SELECT COUNT(r.beneficiario) FROM Resultado r WHERE r.beneficiario = b) = 0')->getResult();

$num_deficientes = count($beneficiariosDeficientes);

$apartamentosTerreo = $entityManager->createQuery('SELECT a FROM Apartamento a WHERE '
                 . 'a.terreo = 1 AND '
                 . '(SELECT COUNT(r.apartamento) FROM Resultado r WHERE r.apartamento = a) = 0')->getResult();

$num_aptos_terreo = count($apartamentosTerreo);

if($num_aptos_terreo > $num_deficientes and $num_deficientes > 0) {
    foreach($beneficiariosDeficientes as $deficiente) {

        $sql = "INSERT INTO resultados (beneficiario_id,apartamento_id,data) "
                . "SELECT ".$deficiente->getId().",(SELECT id FROM apartamentos WHERE terreo = 1 "
                . "AND id NOT IN (SELECT apartamento_id FROM resultados) ORDER BY RAND() LIMIT 1),'".date('Y-m-d H:i:s')."'";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
    }
}


//IDOSOS
$beneficiariosIdosos = $entityManager->createQuery('SELECT b FROM Beneficiario b WHERE b.pcd = \'Não\' '
                 . ' AND b.idoso = \'Sim\' AND '
                 . '(SELECT COUNT(r.beneficiario) FROM Resultado r WHERE r.beneficiario = b) = 0')->getResult();

$num_idosos = count($beneficiariosIdosos);

$apartamentosTerreo = $entityManager->createQuery('SELECT a FROM Apartamento a WHERE '
                 . 'a.terreo = 1 AND '
                 . '(SELECT COUNT(r.apartamento) FROM Resultado r WHERE r.apartamento = a) = 0')->getResult();

$num_aptos_terreo = count($apartamentosTerreo);

if($num_aptos_terreo > $num_idosos and $num_idosos > 0) {
    foreach($beneficiariosIdosos as $idoso) {

        $sql = "INSERT INTO resultados (beneficiario_id,apartamento_id,data) "
                . "SELECT ".$idoso->getId().",(SELECT id FROM apartamentos WHERE terreo = 1 "
                . "AND id NOT IN (SELECT apartamento_id FROM resultados) ORDER BY RAND() LIMIT 1),'".date('Y-m-d H:i:s')."'";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
    }
}

//RESTANTE
$beneficiarios = $entityManager->createQuery('SELECT b FROM Beneficiario b WHERE  '
                 . '(SELECT COUNT(r.beneficiario) FROM Resultado r WHERE r.beneficiario = b) = 0')->getResult();


$apartamentos = $entityManager->createQuery('SELECT a FROM Apartamento a WHERE '
                 . '(SELECT COUNT(r.apartamento) FROM Resultado r WHERE r.apartamento = a) = 0')->getResult();

$num_aptos = count($apartamentos);

foreach($apartamentos as $apartamento) {
    $sql = "INSERT INTO resultados (apartamento_id,beneficiario_id,data) "
            . "SELECT ".$apartamento->getId().",(SELECT id FROM beneficiarios WHERE "
            . "id NOT IN (SELECT beneficiario_id FROM resultados) ORDER BY RAND() LIMIT 1),'".date('Y-m-d H:i:s')."'";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
}

echo '{"sorteado": 1}';