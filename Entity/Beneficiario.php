<?php
// Entity/Beneficiario.php

/**
 * @Entity @Table(name="beneficiarios")
 **/
class Beneficiario
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="string") **/
    protected $nome;
        
    /** @Column(type="string") **/
    protected $nis;
    
    /** @Column(type="string") **/
    protected $cpf;
    
    /** @Column(type="string") **/
    protected $idoso;
    
    /** @Column(type="string") **/
    protected $pcd;
    
    /** @Column(type="string") **/
    protected $reserva;
    
    /** @Column(type="string") **/
    protected $grupo;
    
    /** @Column(type="string") **/
    protected $posicao;
    
     /** @Column(type="string") **/
    protected $telefone;
    
    /**
     * @OneToOne(targetEntity="Resultado", mappedBy="beneficiario")
     */
    private $resultado;
    
     
    public function getId()
    {
        return $this->id;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
    }
    
    function getNis() {
        return $this->nis;
    }

    function getCpf() {
        return $this->cpf;
    }

    function getIdoso() {
        return $this->idoso;
    }

    function getPcd() {
        return $this->pcd;
    }

    function getReserva() {
        return $this->reserva;
    }

    function getGrupo() {
        return $this->grupo;
    }

    function getPosicao() {
        return $this->posicao;
    }

    function getTelefone() {
        return $this->telefone;
    }

    function setNis($nis) {
        $this->nis = $nis;
    }

    function setCpf($cpf) {
        $this->cpf = $cpf;
    }

    function setIdoso($idoso) {
        $this->idoso = $idoso;
    }

    function setPcd($pcd) {
        $this->pcd = $pcd;
    }

    function setReserva($reserva) {
        $this->reserva = $reserva;
    }

    function setGrupo($grupo) {
        $this->grupo = $grupo;
    }

    function setPosicao($posicao) {
        $this->posicao = $posicao;
    }

    function setTelefone($telefone) {
        $this->telefone = $telefone;
    }


    
}