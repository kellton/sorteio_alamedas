<?php
// Entity/Resultado.php

/**
 * @Entity @Table(name="resultados")
 **/
class Resultado
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    
   /**
     * @OneToOne(targetEntity="Apartamento", inversedBy="resultado")
     * @JoinColumn(name="apartamento_id", referencedColumnName="id")
     */
    protected $apartamento;
    
    /**
    * @OneToOne(targetEntity="Beneficiario", inversedBy="resultado")
    * @JoinColumn(name="beneficiario_id", referencedColumnName="id")
    */
    protected $beneficiario;
    
    public function getId()
    {
        return $this->id;
    }
    
    function getBeneficiario() {
        return $this->beneficiario;
    }

    public function setBeneficiario($beneficiario)
    {
        $this->beneficiario = $beneficiario;
    }
    
    function getApartamento() {
        return $this->apartamento;
    }
    
    public function setApartamento($apartamento)
    {
        $this->apartamento = $apartamento;
    }



}