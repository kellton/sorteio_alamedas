<?php
// Entity/Empreendimento.php

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity @Table(name="empreendimentos")
 **/
class Empreendimento
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
     /** @Column(type="string") **/
    protected $nome;
    
    /**
     * @OneToMany(targetEntity="Quadra", mappedBy="empreendimento")
     */
    protected $quadras;
    
    public function __construct()
    {
        $this->quadras = new ArrayCollection();
    }
     
    public function getId()
    {
        return $this->id;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
    }
}