<?php
// Entity/Apartamento.php

/**
 * @Entity @Table(name="apartamentos")
 **/
class Apartamento
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
     /** @Column(type="string") **/
    protected $nome;
     /** @Column(type="boolean") **/
    protected $terreo;
    
    /**
     * @ManyToOne(targetEntity="Empreendimento")
     * @JoinColumn(name="empreendimento_id", referencedColumnName="id")
     */
    protected $empreendimento;
    
    /**
     * @ManyToOne(targetEntity="Quadra")
     * @JoinColumn(name="quadra_id", referencedColumnName="id")
     */
    protected $quadra;
    
    /**
     * @ManyToOne(targetEntity="Lote")
     * @JoinColumn(name="lote_id", referencedColumnName="id")
     */
    protected $lote;
    
    /**
     * @ManyToOne(targetEntity="Bloco")
     * @JoinColumn(name="bloco_id", referencedColumnName="id")
     */
    protected $bloco;
    
    /**
     * @OneToOne(targetEntity="Resultado", mappedBy="apartamento")
     */
    private $resultado;

    public function getId()
    {
        return $this->id;
    }
    
    public function getNome()
    {
        return $this->nome;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
    }
    
    public function getTerreo()
    {
        return $this->terreo;
    }

    public function setTerreo($terreo)
    {
        $this->terreo = $terreo;
    }
    
    /**
     * 
     * @return Bloco
     */
    public function getBloco()
    {
        return $this->bloco;
    }

    public function setBloco(Bloco $bloco)
    {
        $this->bloco = $bloco;
    }
    
    /**
     * 
     * @return Lote
     */
    public function getLote()
    {
        return $this->lote;
    }

    public function setLote(Lote $lote)
    {
        $this->lote = $lote;
    }
    
    /**
     * 
     * @return Quadra
     */
    public function getQuadra()
    {
        return $this->quadra;
    }

    public function setQuadra(Quadra $quadra)
    {
        $this->quadra= $quadra;
    }
    
    /**
     * 
     * @return Empreendimento
     */
    public function getEmpreendimento()
    {
        return $this->empreendimento;
    }

    public function setEmpreendimento(Empreendimento $empreendimento)
    {
        $this->empreendimento = $empreendimento;
    }
}