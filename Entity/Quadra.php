<?php
// Entity/Quadra.php
/**
 * @Entity @Table(name="quadras")
 **/
class Quadra
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
     /** @Column(type="string") **/
    protected $nome;
    
    /**
     * @ManyToOne(targetEntity="Empreendimento")
     * @JoinColumn(name="empreendimento_id", referencedColumnName="id")
     */
    protected $empreendimento;
    
    /**
     * @OneToMany(targetEntity="Lote", mappedBy="quadra")
     */
    protected $lotes;
    
    public function __construct()
    {
        $this->lotes = new ArrayCollection();
    }
     
    public function getId()
    {
        return $this->id;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
    }
    
    /**
     * 
     * @return Empreendimento
     */
    public function getEmpreendimento()
    {
        return $this->empreendimento;
    }

    public function setEmpreendimento(Empreendimento $empreendimento)
    {
        $this->empreendimento = $empreendimento;
    }
            
}