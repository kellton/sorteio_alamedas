<?php
// Entity/Lote.php

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity @Table(name="lotes")
 **/
class Lote
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
     /** @Column(type="string") **/
    protected $nome;
    
    /**
     * @ManyToOne(targetEntity="Empreendimento")
     * @JoinColumn(name="empreendimento_id", referencedColumnName="id")
     */
    protected $empreendimento;
    
    /**
     * @ManyToOne(targetEntity="Quadra")
     * @JoinColumn(name="quadra_id", referencedColumnName="id")
     */
    protected $quadra;
    
    /**
     * @OneToMany(targetEntity="Bloco", mappedBy="lote")
     */
    protected $blocos;
    
    public function __construct()
    {
        $this->blocos = new ArrayCollection();
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
    }
    
    /**
     * 
     * @return Quadra
     */
    public function getQuadra()
    {
        return $this->quadra;
    }

    public function setQuadra(Quadra $quadra)
    {
        $this->quadra= $quadra;
    }
    
    /**
     * 
     * @return Empreendimento
     */
    public function getEmpreendimento()
    {
        return $this->empreendimento;
    }

    public function setEmpreendimento(Empreendimento $empreendimento)
    {
        $this->empreendimento = $empreendimento;
    }
}