<?php
// Entity/Bloco.php

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity @Table(name="blocos")
 **/
class Bloco
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
     /** @Column(type="string") **/
    protected $nome;
    
    /**
     * @ManyToOne(targetEntity="Empreendimento")
     * @JoinColumn(name="empreendimento_id", referencedColumnName="id")
     */
    protected $empreendimento;
    
    /**
     * @ManyToOne(targetEntity="Quadra")
     * @JoinColumn(name="quadra_id", referencedColumnName="id")
     */
    protected $quadra;
    
    /**
     * @ManyToOne(targetEntity="Lote")
     * @JoinColumn(name="lote_id", referencedColumnName="id")
     */
    protected $lote;
    
    /**
     * @OneToMany(targetEntity="Apartamento", mappedBy="bloco")
     */
    protected $apartamentos;
    
    public function __construct()
    {
        $this->apartamentos = new ArrayCollection();
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
    }
    
    /**
     * 
     * @return Lote
     */
    public function getLote()
    {
        return $this->lote;
    }

    public function setLote(Lote $lote)
    {
        $this->lote = $lote;
    }
    
    /**
     * 
     * @return Quadra
     */
    public function getQuadra()
    {
        return $this->quadra;
    }

    public function setQuadra(Quadra $quadra)
    {
        $this->quadra= $quadra;
    }
    
    /**
     * 
     * @return Empreendimento
     */
    public function getEmpreendimento()
    {
        return $this->empreendimento;
    }

    public function setEmpreendimento(Empreendimento $empreendimento)
    {
        $this->empreendimento = $empreendimento;
    }
}