<?php
require_once "bootstrap.php";

// CRIAR PRIMEIRO O EMPREENDIMENTO NA TABELA "EMPREENDIMENTOS"
// DEPOIS CRIAR AS QUADRAS NA TABELA "QUADRAS"
// DEPOIS CRIAR OS LOTES NA TABELA "LOTES"


/* CRIAÇÃO DOS BLOCOS(RODA PRIMEIRO) */

$lotesRepository = $entityManager->getRepository('Lote');

$quantidade = array('17','14','10'); //QUANTIDADES DE BLOCOS POR LOTE
$j=0;
$quantidaBlocos=0;
$contador=0;
$todosLotes=$lotesRepository->findAll();
foreach($todosLotes as $linha){
    $lote = $entityManager->find('Lote',$linha->getId());
    for($i=1; $i<=$quantidade[$j];$i++) {
        $bloco = new Bloco();
        /*if(($i>5 && $linha->getId()==1) || ($i>8 && $linha->getId()==2 || $linha->getId()==8)){
            if($linha->getId()==1){
                $contador=$i+7;
            }
            if($linha->getId()==2){
                $contador=$i+4;
            }
            if($linha->getId()==8){
                $contador=$i+3;
            }
            $bloco->setNome('BLOCO '.  str_pad($contador, 2,'0',STR_PAD_LEFT));
        }
        else{*/
            $bloco->setNome('BLOCO '.  str_pad($i, 2,'0',STR_PAD_LEFT));
        /*}*/
        $bloco->setLote($lote);
        $bloco->setQuadra($lote->getQuadra());
        $bloco->setEmpreendimento($lote->getQuadra()->getEmpreendimento());
    
        $entityManager->persist($bloco);
        $entityManager->flush();
        $quantidaBlocos++;
    }
    $j++;
}

echo "Geração de ".$quantidaBlocos." Blocos Feita Com Sucesso!!!<br/>";

/* FIM DA GERAÇÃO DE BLOCOS*/

$blocosRepository = $entityManager->getRepository('Bloco');
$blocos = $blocosRepository->findAll();

$quantidaApartamentos=0;

/* CRIAÇÃO DOS APARTAMENTOS */
foreach($blocos as $bloco) {
    for($i=1;$i<=4;$i++) {
        for($j=1;$j<=4;$j++) {
            $apartamento = new Apartamento();
            $apartamento->setBloco($bloco);
            $apartamento->setLote($bloco->getLote());
            $apartamento->setQuadra($bloco->getQuadra());
            $apartamento->setEmpreendimento($bloco->getEmpreendimento());
            $apartamento->setNome('APARTAMENTO '.$i.'0'.$j);
            if($i==1) {
                $apartamento->setTerreo(1);
            } else {
                $apartamento->setTerreo(0);
            }
            $entityManager->persist($apartamento);
            $entityManager->flush();
            $quantidaApartamentos++;
        }
    }
}

echo "Geração de ".$quantidaApartamentos." Apartamentos Feita Com Sucesso!!!";

/* FIM */