<!DOCTYPE html>
<?php
require_once "bootstrap.php";


$apartamentosRepository = $entityManager->getRepository('Apartamento');
$apartamentos = $apartamentosRepository->findAll();


$beneficiariosRepository = $entityManager->getRepository('Beneficiario');
$beneficiarios = $beneficiariosRepository->findAll();

$resultadosRepository = $entityManager->getRepository('Resultado');
$resultados = $resultadosRepository->findAll();

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>LOTEAMENTO CIDADE JARDIM 2</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="bootstrap/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="bootstrap/css/cover.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<style>
	.loader {
			margin: auto;
		  border: 16px solid #f3f3f3;
		  border-radius: 50%;
		  border-top: 16px solid #3498db;
		  width: 120px;
		  height: 120px;
		  -webkit-animation: spin 2s linear infinite; /* Safari */
		  animation: spin 2s linear infinite;
	}

	/* Safari */
	@-webkit-keyframes spin {
	  0% { -webkit-transform: rotate(0deg); }
	  100% { -webkit-transform: rotate(360deg); }
	}

	@keyframes spin {
	  0% { transform: rotate(0deg); }
	  100% { transform: rotate(360deg); }
	}
</style>
  </head>

  <body>

    <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">

          <div class="masthead clearfix">
            <div class="inner">
                <a href="index.php"><img src="img/prefeitura.png" style="height: 100px; margin: auto;" /></a>
            </div>
          </div>

          <div class="inner cover">
              <h1 class="cover-heading">LOTEAMENTO CIDADE JARDIM 2</h1>
              <br />
              <br />
          <?php if(count($resultados) == 0) { ?>  
            <p class="lead">
              <a href="#" class="btn btn-lg btn-default" id="iniciar_sorteio">Lotear</a>
			  <div class="loader" id="spinner" style="display:none;"></div>              
            </p>
            
            <a href="http://localhost/sorteio/resultado/gerar-pdf" class="btn btn-lg btn-default btn-block" id="visualizar_sorteio" style="display:none;" target="_blank">Visualizar Resultado do Loteamento (Lista)</a>
            <a href="http://localhost/sorteio/resultado/slide" class="btn btn-lg btn-default btn-block" id="visualizar_sorteio_slide" style="display:none;" target="_blank">Visualizar Resultado do Loteamento (Slide)</a>
            <div id="sucesso" style="display: none;">
                <h3>Loteamento Realizado com Sucesso!</h3>
            </div>
            <?php } else { ?>
            <a href="http://localhost/sorteio/resultado/gerar-pdf" class="btn btn-lg btn-default btn-block" id="visualizar_sorteio" target="_blank">Visualizar Resultado do Loteamento (Lista)</a>
            <a href="http://localhost/sorteio/resultado/slide" class="btn btn-lg btn-default btn-block" id="visualizar_sorteio_slide" target="_blank">Visualizar Resultado do Loteamento (Slide)</a>
            <div id="sucesso">
                <h3>Loteamento Realizado com Sucesso!</h3>
            </div>
            <?php }  ?>
          </div>
            <div class="mastfoot" style="height: 78px;">
            <p>Desenvolvido pela: <a href="https://www.fortaleza.ce.gov.br/institucional/a-secretaria-337" target="_blank">Secretaria Municipal do Desenvolvimento Habitacional de Fortaleza – H A B I T A F O R</a></p>
            <div style="background-image:url('img/linha2.png'); background-repeat:repeat-x; height: 38px;">  
            </div>
        </div>

        </div>

      </div>

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="bootstrap/js/vendor/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="bootstrap/js/ie10-viewport-bug-workaround.js"></script>
    <script type="text/javascript">
	
			var sorteado = 0;

			$('#iniciar_sorteio').click(function(){
				
                                $('#iniciar_sorteio').hide();
                                $('#spinner').show();
                                
				$.ajax({
					url:'sortear.php',
					method: 'GET',
					dataType: 'json',
					success: function(data) {
                                            
                                            
						
						$('#spinner').show();
						
						if(data.sorteado == 1) {
							//$('#iniciar_sorteio').click(function(){ return;});
							$('#iniciar_sorteio').hide();
							sorteado = 1;
                                                        
                                                        $('#spinner').hide();
								
                                                        $('#visualizar_sorteio').show();
                                                        $('#visualizar_sorteio_slide').show();
                                                        $('#sucesso').show();					
							
						}

						
					},
                                        error: function() {
                                            
                                            $('#spinner').hide();
                                            $('#iniciar_sorteio').show();
                                        }
					
				});
				
			});



        </script>
  </body>
</html>
